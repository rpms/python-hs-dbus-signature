%global srcname hs-dbus-signature

Name:           python-%{srcname}
Version:        0.05
Release:        3%{?dist}
Summary:        Hypothesis Strategy for Generating Arbitrary DBus Signatures

License:        MPLv2.0
URL:            https://github.com/stratis-storage/hs-dbus-signature
Source0:        %{url}/archive/v%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%global _description \
This package contains a Hypothesis strategy for generating DBus signatures.\
\
The strategy is intended to be both sound and complete. That is, it should\
never generate an invalid DBus signature and it should be capable,\
modulo size constraints, of generating any valid DBus signature.

%description %{_description}

%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pytest
BuildRequires:  python3-hypothesis
Requires:       python3-hypothesis

%description -n python3-%{srcname} %{_description}

Python 3 version.

%prep
%autosetup -n %{srcname}-%{version}

%build
%py3_build

%install
%py3_install

%check
PYTHONPATH=%{buildroot}%{python3_sitelib} pytest-%{python3_version} -v tests

%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/hs_dbus_signature/
%{python3_sitelib}/hs_dbus_signature-*.egg-info/

%changelog
* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.05-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Mon Jan 08 2018 Ilya Gradina <ilya.gradina@gmail.com> - 0.05-2
- Minor changes

* Mon Jan 08 2018 Ilya Gradina <ilya.gradina@gmail.com> - 0.05-1
- Initial package
